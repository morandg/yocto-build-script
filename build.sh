#!/bin/bash
#
# Yocto build helper script
#
# Copyright (C) 2018 R4nd0m6uy
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
set -e

PROJECT_BASE=$(pwd)
BUILD_DIRECTORY=$(pwd)/build
META_LAYERS_DIR=${PROJECT_BASE}/meta-layers
POKY_BASE=${META_LAYERS_DIR}/poky
META_LAYERS_FILE=meta_layers
DEFAULT_MACHINE="raspberrypi3"
DEFAULT_RECIPE="core-image-minimal"
BUILD_MACHINE=${DEFAULT_MACHINE}
BUILD_RECIPE=${DEFAULT_RECIPE}
IS_BUILD_SDK=false

########################################################################
usage()
{
  echo "${0} [ -h | -u | -i | -r ]"
  echo "-h    Shows this help"
  echo "-u    Update meta-layers repositories"
  echo "-i    Init poky environment"
  echo "-s    Build an SDK"
  echo "-r    Recipe to build (${DEFAULT_RECIPE})"
  echo "-m    Machine to build for (${DEFAULT_MACHINE})"
}

########################################################################
update_meta_layers()
{
  echo "Updating meta-layers ..."
  cd ${PROJECT_BASE}

  for layer in $(grep -v ^# ${META_LAYERS_FILE})
  do
    cd ${PROJECT_BASE}
    META_PATH=$(echo ${layer} | cut -d\; -f1)
    META_URL=$(echo ${layer} | cut -d\; -f2)
    META_REV=$(echo ${layer} | cut -d\; -f3)
    # Make sure we have correct entry
    if [ "${META_PATH}" = "" -o "${META_URL}" = "" -o ${META_REV} = "" ]
    then
      echo "Malformed meta-layer line:"
      echo ${layer}
      exit -1
    fi
  
    if [ ! -d ${META_LAYERS_DIR}/${META_PATH} ]
    then
      git clone ${META_URL} ${META_LAYERS_DIR}/${META_PATH}
    fi
    cd ${META_LAYERS_DIR}/${META_PATH}
    git fetch origin
    git checkout ${META_REV}
    git pull origin ${META_REV}
  done

  echo "meta-layers up to date!"
}

########################################################################
init_poky_env()
{
  if [ ! -d ${POKY_BASE} ]
  then
    update_meta_layers
    if [ ! -d ${POKY_BASE} ]
    then
      echo "Poky base directory \"${POKY_BASE}\" not found!"
      exit 1
    fi
  fi

  # Remove existing configuration if exists and user want to
  echo "Initializing poky environement ..."
  cd ${PROJECT_BASE}
  if [ -d ${BUILD_DIRECTORY}/conf ]
  then
    echo -n "A poky environement already exists, would you like to remove it "
    echo "and restart again? [y/N]"
    read REMOVE_ENV
    REMOVE_ENV=$(echo $REMOVE_ENV | head -c1)
    if [ ${REMOVE_ENV} = "y" -o ${REMOVE_ENV} = "Y" ]
    then
      rm -rf ${BUILD_DIRECTORY}/conf
    else
      return
    fi
  fi

  # Initialize the environment
  TEMPLATECONF="${META_LAYERS_DIR}/meta-r4nd0m6uy/conf"
  . ${POKY_BASE}/oe-init-build-env ${BUILD_DIRECTORY} > /dev/null
  echo "Your poky environment is ready!"
}

########################################################################
build_recipe()
{
  [ ! -d ${BUILD_DIRECTORY}/conf ] && init_poky_env
  ${IS_BUILD_SDK} && BITBAKE_EXTRA_ARGS="-c populate_sdk"

  . ${POKY_BASE}/oe-init-build-env ${BUILD_DIRECTORY} > /dev/null
  MACHINE=${BUILD_MACHINE} bitbake ${BITBAKE_EXTRA_ARGS} ${BUILD_RECIPE}
}

########################################################################
# Main script
while getopts "huisr:m:" FLAG; do
  case $FLAG in
    h)
      usage
      exit 0
      ;;
    u)
      update_meta_layers
      exit 0
      ;;
    i)
      init_poky_env
      exit 0
      ;;
    s)
      IS_BUILD_SDK=true
      ;;
    r)
      BUILD_RECIPE=${OPTARG}
      ;;
    m)
      BUILD_MACHINE=${OPTARG}
      ;;
    \?)
      usage
      exit 1
      ;;
  esac
done

build_recipe
