#!/bin/sh
#
# Enter the docker container for debugging purpose
########################################################################
CONTAINER_NAME=yocto-build
USER_ID=$(id -u ${USER})
GROUP_ID=$(id -g ${USER})

build_container() {
  docker build \
    --build-arg USER_ID=${USER_ID} \
    --build-arg GROUP_ID=${GROUP_ID} \
    -t ${CONTAINER_NAME} docker/
}

run_container() {
  build_container
  docker run \
    -it \
    --rm \
    ${2} \
    -v $(pwd):/opt/yocto-build-script \
    --user ${USER_ID}:${GROUP_ID} \
    ${CONTAINER_NAME} \
    /bin/bash
}

run_container
