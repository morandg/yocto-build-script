FROM debian:buster

ARG USER_ID=1000
ARG GROUP_ID=1000

RUN apt-get update
RUN apt-get upgrade -y
RUN apt-get -y install --fix-missing \
  gawk \
  wget \
  git-core \
  diffstat \
  unzip \
  texinfo \
  gcc-multilib \
  build-essential \
  chrpath \
  socat \
  cpio \
  python3 \
  python3-pip \
  python3-pexpect \
  xz-utils \
  debianutils \
  iputils-ping \
  python3-git \
  python3-jinja2 \
  libegl1-mesa \
  libsdl1.2-dev \
  pylint3 \
  xterm \
  u-boot-tools \
  jq \
  curl \
  locales \
  subversion

# Generate some locales
RUN sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen && \
  locale-gen
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

RUN groupadd --gid ${GROUP_ID} yocto && \
  adduser --uid ${USER_ID} --ingroup yocto --disabled-password --system --home /home/yocto --shell /bin/bash yocto
